%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "creationTime": |2021-06-24T15:53:09|,
  "recordCount": 7,
  "result": {
    "onCompletePhaseException": null,
    "loadingPhaseException": null,
    "totalRecords": 7,
    "elapsedTimeInMillis": 0,
    "failedOnCompletePhase": false,
    "loadedRecords": 7,
    "failedRecords": 0,
    "failedOnInputPhase": false,
    "successfulRecords": 0,
    "inputPhaseException": null,
    "processedRecords": 0,
    "failedOnLoadingPhase": false,
    "batchJobInstanceId": "2c44d350-d4d6-11eb-8bdd-30c9aba719fa"
  },
  "id": "2c44d350-d4d6-11eb-8bdd-30c9aba719fa",
  "ownerJobName": "processapidb-salesBatch_Job",
  "status": "EXECUTING"
})